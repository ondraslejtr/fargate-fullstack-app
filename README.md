# Fargate Full Stack 

Independently Scalable Multi-Container Microservices Architecture on AWS Fargate under ECS, specified using CloudFormation, contains:

* Reverse proxy (nginx), public facing
* JavaScript Express frontend reachable through proxy
* Python Flask backend running mock "expensive" computation

All pieces are containerized with their own Dockerfile and are easily runnable both locally (docker-compose) and separatedly (good old single docker images).

## Networking 

Application use URLs similar to `http://ecsfs-backend.local`, which will work both locally using docker-compose (thanks to network aliases) and in cloud (because of AWS service naming structure).

## Running

* Install aws-cli utility: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
* Configure it: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.ht
* Build local images using included Dockerfiles and push them to Docker Hub so AWS can pull them.

* Create and run stack: 

```bash
aws cloudformation create-stack \
  --stack-name ecsfs \
  --template-body file://$(pwd)/stack.yaml \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameters ParameterKey=HostedZoneName,ParameterValue=
```

* Destroy stack (wait until all services are spin-up)

```bash
aws cloudformation delete-stack  – stack-name ecsfs
```

Rever to stack.yaml (CloudFormation stack definition file) for more details.

## Overview

![AWS Diagram](diagram.png)

Information and walkthrough coming soon.
